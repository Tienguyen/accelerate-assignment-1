﻿using Accelerate_assignment1.Heroes.Classes;
using Accelerate_assignment1.Heroes.Items;
using Accelerate_assignment1.Items;
using Accelerate_assignment1.utility;
using System;
using System.Collections.Generic;

namespace Accelerate_assignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            Mage mage = new Mage("p1");
            mage.LevelUp(50);
            Console.WriteLine(mage.Level);
            Console.WriteLine("DPS NO WEAPON" + DPSCalculator.CalculateHeroDPS(mage));

            Weapon staff = new Weapon("rod", 5, WeaponTypes.Staves, 2000.0, 2.0);
            Weapon sword = new Weapon("sworda", 5, WeaponTypes.Swords, 7000.0, 1.0);
            Armor robe = new Armor("wizzrobe", 10, ArmorTypes.Cloth, SlotTypes.Body, 5, 8, 98, 25);
            Armor hat = new Armor("Dcap", 10, ArmorTypes.Cloth, SlotTypes.Head, 3, 2, 180, 40);
            mage.EquipWeapon(staff);
            mage.DisplayStats();
        }
    }
}
