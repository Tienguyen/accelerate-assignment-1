﻿using Accelerate_assignment1.Heroes;
using Accelerate_assignment1.Heroes.Items;
using Accelerate_assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelerate_assignment1.utility
{
    public static class DPSCalculator
    {
        /// <summary>
        /// Calculates a weapons DPS by multiplying its base damage with its attack speed
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>The weapons dps</returns>
        public static double CalculateWeaponDPS(Weapon weapon)
        {
            double result = weapon.BaseDamage * weapon.AttackSpeed;
            return result;
        }

        /// <summary>
        /// Calculates a hero's DPS by the formula:
        /// CurrentlyEquipedWeaponDPS * (1+TotalPrimaryAttributes)/100
        /// 
        /// Returns 1.0 if no weapon is equiped (default weapon means no weapon equiped)
        /// </summary>
        /// /// <param name="hero"></param>
        /// <returns>The hero's total dps</returns>

        public static double CalculateHeroDPS(Hero hero)
        {
            Weapon heroWeapon = hero.GetWeapon();
            double weaponDPS = CalculateWeaponDPS(heroWeapon);
            int totalPrimaryAttribute = hero.TotalPrimaryAttributes.GetStat(hero.MainStat);
            double totalDPS = weaponDPS * ((1+ totalPrimaryAttribute) /100);
            return totalDPS;
        }
    }
}
