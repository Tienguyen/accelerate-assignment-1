﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelerate_assignment1.Heroes
{
    public class SecondaryAttributes
    {
        public int Health { get; set; }
        public int ArmorRating { get; set; }
        public int ElementalResistance { get; set; }

        public SecondaryAttributes(int health, int armorRating, int elementalResistance)
        {
            Health = health;
            ArmorRating = armorRating;
            ElementalResistance = elementalResistance;
        }

        //Override Equals method to work with testing
        public override bool Equals(object obj)
        {
            return obj is SecondaryAttributes attributes &&
                   Health == attributes.Health &&
                   ArmorRating == attributes.ArmorRating &&
                   ElementalResistance == attributes.ElementalResistance;
        }

        //Override GetHashCode method to work with testing
        public override int GetHashCode()
        {
            return HashCode.Combine(Health, ArmorRating, ElementalResistance);
        }
    }
}
