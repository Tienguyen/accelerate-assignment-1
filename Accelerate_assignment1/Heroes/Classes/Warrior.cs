﻿using Accelerate_assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelerate_assignment1.Heroes.Classes
{
    public class Warrior : Hero
    {
        public Warrior(string name) : base(name)
        {

            BasePrimaryAttributes = new PrimaryAttributes(5, 2, 1, 10);
            TotalPrimaryAttributes = new PrimaryAttributes(5, 2, 1, 10);
            UpdateStats();
        }

        public override int[] StatGainPerLevel
        {
            get { return new int[] { 3, 2, 1, 5 }; }
        }

        public override List<WeaponTypes> ClassEquippableWeaponsTypes
        {
            get { return new List<WeaponTypes> { WeaponTypes.Swords, WeaponTypes.Hammers, WeaponTypes.Axes }; }
        }

        public override List<ArmorTypes> ClassEquippableArmorTypes
        {
            get { return new List<ArmorTypes> { ArmorTypes.Mail, ArmorTypes.Plate }; }
        }


        public override PrimaryStatTypes MainStat
        {
            get { return PrimaryStatTypes.Strength; }
        }

    }
}
