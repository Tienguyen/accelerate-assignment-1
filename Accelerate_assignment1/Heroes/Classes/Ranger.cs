﻿using Accelerate_assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelerate_assignment1.Heroes.Classes
{
    public class Ranger : Hero
    {
        public Ranger(string name) : base(name)
        {

            BasePrimaryAttributes = new PrimaryAttributes(1, 7, 1, 8);
            TotalPrimaryAttributes = new PrimaryAttributes(1, 7, 1, 8);
            UpdateStats();
        }

        public override int[] StatGainPerLevel
        {
            get { return new int[] { 1, 5, 1, 2 }; }
        }

        public override List<WeaponTypes> ClassEquippableWeaponsTypes
        {
            get { return new List<WeaponTypes> { WeaponTypes.Bows }; }
        }

        public override List<ArmorTypes> ClassEquippableArmorTypes
        {
            get { return new List<ArmorTypes> { ArmorTypes.Leather, ArmorTypes.Mail }; }
        }

        public override PrimaryStatTypes MainStat
        {
            get { return PrimaryStatTypes.Dexterity; }
        }
    }
}
