﻿using Accelerate_assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelerate_assignment1.Heroes.Classes
{
    public class Rouge : Hero
    {
        public Rouge(string name) : base(name)
        {

            BasePrimaryAttributes = new PrimaryAttributes(2, 6, 1, 8);
            TotalPrimaryAttributes = new PrimaryAttributes(2, 6, 1, 8);
            UpdateStats();
        }

        public override int[] StatGainPerLevel
        {
            get { return new int[] { 1, 4, 1, 3 }; }
        }

        public override List<WeaponTypes> ClassEquippableWeaponsTypes
        {
            get { return new List<WeaponTypes> { WeaponTypes.Daggers, WeaponTypes.Swords}; }
        }

        public override List<ArmorTypes> ClassEquippableArmorTypes
        {
            get { return new List<ArmorTypes> { ArmorTypes.Leather, ArmorTypes.Mail }; }
        }

        public override PrimaryStatTypes MainStat
        {
            get { return PrimaryStatTypes.Dexterity; }
        }
    }
}
