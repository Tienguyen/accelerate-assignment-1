﻿using Accelerate_assignment1.Items;
using System;
using System.Collections.Generic;

namespace Accelerate_assignment1.Heroes.Classes
{
    public class Mage : Hero
    {
        public List<WeaponTypes> ClassEquipableWeapons;

        public Mage(string name) : base(name) {

            BasePrimaryAttributes = new PrimaryAttributes(1, 1, 8, 5);
            TotalPrimaryAttributes = new PrimaryAttributes(1, 1, 8, 5);
            UpdateStats();
        }

        public override int[] StatGainPerLevel
        {
            get { return new int[] { 1, 1, 5, 3 }; }
        }

        public override List<WeaponTypes> ClassEquippableWeaponsTypes
        {
            get { return new List<WeaponTypes> { WeaponTypes.Staves, WeaponTypes.Wands }; }
        }

        public override List<ArmorTypes> ClassEquippableArmorTypes
        {
            get { return new List<ArmorTypes> { ArmorTypes.Cloth}; }
        }

        public override PrimaryStatTypes MainStat
        {
            get { return PrimaryStatTypes.Intelligence;  }
        }


    }
}
