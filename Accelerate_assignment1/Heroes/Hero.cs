﻿using Accelerate_assignment1.Heroes.Items;
using Accelerate_assignment1.Items;
using Accelerate_assignment1.utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelerate_assignment1.Heroes
{
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes { get; set; }
        public SecondaryAttributes SecondaryAttributes { get; set; }

        public abstract PrimaryStatTypes MainStat { get; }
        public abstract int[] StatGainPerLevel { get; }
        public abstract List<WeaponTypes> ClassEquippableWeaponsTypes { get; }
        public abstract List<ArmorTypes> ClassEquippableArmorTypes { get; }

        public Dictionary<SlotTypes, Item> Equipment = new Dictionary<SlotTypes, Item>();



        public Hero(string Name)
        {
            this.Name = Name;
            Level = 1;
            Equipment.Add(SlotTypes.Head, DefaultItems.defaultHead);
            Equipment.Add(SlotTypes.Body, DefaultItems.defaultBody);
            Equipment.Add(SlotTypes.Legs, DefaultItems.defaultLegs);
            Equipment.Add(SlotTypes.Weapon, DefaultItems.defaultWeapon);
        }

        /// <summary>
        /// Level up the hero once
        /// </summary>
        public void LevelUp()
        {
            Level += 1;
            BasePrimaryAttributes.AddStats(StatGainPerLevel);
            UpdateStats();
        }

 
        /// <summary>
        /// Level up the hero multiple times. Input has to be a positive integer,
        /// otherwise the method throws an argument exception
        /// </summary>
        /// <param name="levels"></param>
        public void LevelUp(int levels)
        {
            if (levels <= 0)
            {
                throw new ArgumentException("The input for LevelUp has to be a positive number!");
            }
            for (int i = 0; i < levels; i++)
            {
                Console.WriteLine("DING");
                Level ++;
                BasePrimaryAttributes.AddStats(StatGainPerLevel);
            }
            UpdateStats();

        }


        /// <summary>
        /// Updates the secondary attributes based on the total primary attributes
        /// </summary>
        public void UpdateSecondaryAttributes()
        {
            int health = TotalPrimaryAttributes.GetStat(PrimaryStatTypes.Vitality) * 10;
            int armorRating = TotalPrimaryAttributes.GetStat(PrimaryStatTypes.Strength)
                + TotalPrimaryAttributes.GetStat(PrimaryStatTypes.Dexterity);
            int elementalResistance = TotalPrimaryAttributes.GetStat(PrimaryStatTypes.Intelligence);
            SecondaryAttributes = new SecondaryAttributes(health, armorRating, elementalResistance);
        }

        /// <summary>
        /// Updates the total primary Attributes based on base primary attributes and armor equipped. 
        /// </summary>
        public void UpdateTotalPrimaryAttributes()
        {
            Dictionary<string, int> stats = new Dictionary<string, int>();
            stats["strength"] = BasePrimaryAttributes.GetStat(PrimaryStatTypes.Strength);
            stats["dexterity"] = BasePrimaryAttributes.GetStat(PrimaryStatTypes.Dexterity);
            stats["intelligence"] = BasePrimaryAttributes.GetStat(PrimaryStatTypes.Intelligence);
            stats["vitality"] = BasePrimaryAttributes.GetStat(PrimaryStatTypes.Vitality);
            foreach (var item in Equipment)
            {
                if(item.Key != SlotTypes.Weapon)
                {
                    Armor armor = (Armor)item.Value;
                    stats["strength"] += armor.BasePrimaryAttributes.GetStat(PrimaryStatTypes.Strength);
                    stats["dexterity"] += armor.BasePrimaryAttributes.GetStat(PrimaryStatTypes.Dexterity);
                    stats["intelligence"] += armor.BasePrimaryAttributes.GetStat(PrimaryStatTypes.Intelligence);
                    stats["vitality"] += armor.BasePrimaryAttributes.GetStat(PrimaryStatTypes.Vitality);
                }
            }

            TotalPrimaryAttributes = new PrimaryAttributes(stats["strength"],
                stats["dexterity"], stats["intelligence"], stats["vitality"]);
        }

        /// <summary>
        /// Updates total primary attributes and secondary attributes
        /// </summary>
        public void UpdateStats()
        {
            UpdateTotalPrimaryAttributes();
            UpdateSecondaryAttributes();

        }

        /// <summary>
        /// Removes the item in the Equipment property at given slot type
        /// </summary>
        /// <param name="slotType"></param>
        public void Unequip(SlotTypes slotType)
        {

            switch (slotType)
            {
                case SlotTypes.Weapon:
                    Equipment[SlotTypes.Weapon] = DefaultItems.defaultWeapon;
                    break;
                case SlotTypes.Head:
                    Equipment[SlotTypes.Head] = DefaultItems.defaultHead;
                    UpdateStats();
                    break;
                case SlotTypes.Body:
                    Equipment[SlotTypes.Body] = DefaultItems.defaultBody;
                    UpdateStats();
                    break;
                case SlotTypes.Legs:
                    Equipment[SlotTypes.Legs] = DefaultItems.defaultLegs;
                    UpdateStats();
                    break;
            }
        }

        /// <summary>
        /// Adds a weapon in the weapon slot of the Equipment property
        /// Throws InvalidWeaponException if the hero equips an incompatible weapon type or is too low level
        /// </summary>
        /// <param name="weapon"></param>
        public string EquipWeapon(Weapon weapon)
        {
            if (ClassEquippableWeaponsTypes.Contains(weapon.WeaponType) || weapon.WeaponType == WeaponTypes.Empty)
            {
                if (weapon.RequiredLevel > Level)
                {
                    throw new InvalidWeaponException("You do not meet the level requirement of level "
                        + weapon.RequiredLevel + "Your level is " + Level);
                }
                else
                {
                    Equipment[weapon.EquipmentSlot] = weapon;
                }
            }

            else
            {
                throw new InvalidWeaponException("Your class cannot equip weapon of type "
                    + weapon.WeaponType);
            }

            return "New weapon equipped!";
        }


        /// <summary>
        /// Adds armor to the given slot type in the Equipment property
        /// /// Throws InvalidArmorException if the hero equips an incompatible armor type or is too low level
        /// </summary>
        /// <param name="armor"></param>
        public string EquipArmor(Armor armor)
        {
            if (ClassEquippableArmorTypes.Contains(armor.ArmorType) || armor.ArmorType == ArmorTypes.Empty)
            {
                if (armor.RequiredLevel > Level)
                {
                    throw new InvalidWeaponException("You do not meet the level requirement of level "
                        + armor.RequiredLevel + "Your level is " + Level);
                }
                else
                {
                    Equipment[armor.EquipmentSlot] = armor;
                    UpdateStats();
                }
            }

            else
            {
                throw new InvalidWeaponException("Your class cannot equip armor of type "
                    + armor.ArmorType);
            }
            return "New armor equipped!";
        }

        /// <summary>
        /// Takes item slot as input and returns the item in that slot
        /// </summary>
        /// <param name="slot"></param>
        /// <returns>the item in given slot</returns>
        public Item GetItem(SlotTypes slot)
        {
            Item result = Equipment[slot];
            return result;

        }

        /// <summary>
        /// Returns the currently equipped weapon
        /// </summary>
        /// <returns>Returns the currently equipped weapon</returns>
        public Weapon GetWeapon()
        {
            Weapon weapon = (Weapon)Equipment[SlotTypes.Weapon];
            return weapon;
        }
        /// <summary>
        /// Writes a summary of the hero's stats in the console
        /// </summary>
        public void DisplayStats()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append($"Hero Name: {Name}\n");
            stringBuilder.Append($"Level: {Level}\n");
            stringBuilder.Append($"Strength: {TotalPrimaryAttributes.GetStat(PrimaryStatTypes.Strength)}\n");
            stringBuilder.Append($"Dexterity: {TotalPrimaryAttributes.GetStat(PrimaryStatTypes.Dexterity)}\n");
            stringBuilder.Append($"Intelligence: {TotalPrimaryAttributes.GetStat(PrimaryStatTypes.Intelligence)}\n");
            stringBuilder.Append($"Health: {SecondaryAttributes.Health}\n");
            stringBuilder.Append($"Armor Rating: {SecondaryAttributes.ArmorRating}\n");
            stringBuilder.Append($"Elemental Resisistance: {SecondaryAttributes.ElementalResistance}\n");
            stringBuilder.Append($"DPS: {DPSCalculator.CalculateHeroDPS(this)})\n");
            Console.Write(stringBuilder.ToString());
        }
    }

}
