﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelerate_assignment1.Heroes
{
    public enum PrimaryStatTypes
    {
        Strength,
        Dexterity,
        Intelligence,
        Vitality
    }
    public class PrimaryAttributes
    {

        public Dictionary<PrimaryStatTypes, int> Stats;

        public PrimaryAttributes(int strength, int dexterity, int intelligence, int vitality)
        {
            Stats = new Dictionary<PrimaryStatTypes, int>();
            Stats[PrimaryStatTypes.Strength] = strength;
            Stats[PrimaryStatTypes.Dexterity] = dexterity;
            Stats[PrimaryStatTypes.Intelligence] = intelligence;
            Stats[PrimaryStatTypes.Vitality] = vitality;
        }


        /// <summary>
        ///Adds stats with the input format [Strength, Dexterity, Intelligence, Vitality]
        /// </summary>
        public void AddStats(int[] statGains)
        {
            Stats[PrimaryStatTypes.Strength] += statGains[0];
            Stats[PrimaryStatTypes.Dexterity] += statGains[1];
            Stats[PrimaryStatTypes.Intelligence] += statGains[2];
            Stats[PrimaryStatTypes.Vitality] += statGains[3];
        }

        //Override Equals method to work with testing
        public override bool Equals(object obj)
        {
            return obj is PrimaryAttributes attributes &&
                   Stats[PrimaryStatTypes.Strength] == attributes.Stats[PrimaryStatTypes.Strength] &&
                   Stats[PrimaryStatTypes.Dexterity] == attributes.Stats[PrimaryStatTypes.Dexterity] &&
                   Stats[PrimaryStatTypes.Intelligence] == attributes.Stats[PrimaryStatTypes.Intelligence] &&
                   Stats[PrimaryStatTypes.Vitality] == attributes.Stats[PrimaryStatTypes.Vitality];
        }

        //Override GetHashCode method to work with testing
        public override int GetHashCode()
        {
            return HashCode.Combine(Stats);
        }


        /// <summary>
        /// Returns the given stat
        /// </summary>
        /// <param name="stat"></param>
        /// <returns>Returns the given stat</returns>
        public int GetStat(PrimaryStatTypes stat)
        {
            return Stats[stat];
        }
    }
}
