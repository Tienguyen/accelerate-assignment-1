﻿using Accelerate_assignment1.Heroes.Items;
using Accelerate_assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelerate_assignment1.Heroes
{
    // Default items are used when no other items are equiped in the respective slot.
    public static class DefaultItems
    {
        //The DPS of the defaultWeapon will be 1.0
        public static Weapon defaultWeapon = new Weapon("defaultWeapon", 0, WeaponTypes.Empty, 1.0, 1.0);
        public static Armor defaultHead = new Armor("defaultHead", 0, ArmorTypes.Empty, SlotTypes.Head, 0, 0, 0, 0);
        public static Armor defaultBody = new Armor("defaultBody", 0, ArmorTypes.Empty, SlotTypes.Body, 0, 0, 0, 0);
        public static Armor defaultLegs = new Armor("defaultLegs", 0, ArmorTypes.Empty, SlotTypes.Legs, 0, 0, 0, 0);


    }
}
