﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelerate_assignment1.Heroes.Items
{
    public enum SlotTypes
    {
        Head,
        Body,
        Legs,
        Weapon
    }
    public abstract class Item
    {
        public string Name;
        public int RequiredLevel;
        public abstract SlotTypes EquipmentSlot { get; }


        public Item(string Name, int RequiredLevel)
        {
            this.Name = Name;
            this.RequiredLevel = RequiredLevel;
        }
    }

}
