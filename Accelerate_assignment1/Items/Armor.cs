﻿using Accelerate_assignment1.Heroes;
using Accelerate_assignment1.Heroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelerate_assignment1.Items
{
    public enum ArmorTypes
    {
        Cloth,
        Leather,
        Mail,
        Plate,
        Empty
    }



    public class Armor : Item
    {
        public ArmorTypes ArmorType { get; set; }

        public SlotTypes Slot;

        public PrimaryAttributes BasePrimaryAttributes { get; set; }

        public override SlotTypes EquipmentSlot
        {
            get { return Slot; }
        }

        public Armor(string Name, int RequiredLevel, ArmorTypes ArmorType, SlotTypes Slot,
            int strength, int dexterity, int intelligence, int vitality) : base(Name, RequiredLevel)
        {
            BasePrimaryAttributes = new PrimaryAttributes(strength, dexterity, intelligence, vitality);
            this.ArmorType = ArmorType;
            this.Slot = Slot;
        }


    }
}
