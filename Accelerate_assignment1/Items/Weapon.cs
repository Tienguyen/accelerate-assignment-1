﻿using Accelerate_assignment1.Heroes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelerate_assignment1.Items
{
    public enum WeaponTypes
    {
        Axes,
        Bows,
        Daggers,
        Hammers,
        Staves,
        Swords,
        Wands,
        Empty
    }
    public class Weapon : Item
    {
        public WeaponTypes WeaponType { get; set; }
        public double BaseDamage { get; set; }
        public double AttackSpeed { get; set; }

        public override SlotTypes EquipmentSlot
        {
            get { return SlotTypes.Weapon; }
        }

        public Weapon(string Name, int RequiredLevel, WeaponTypes WeaponType,
            double BaseDamage, double AttackSpeed) : base(Name, RequiredLevel)
        {
            this.WeaponType = WeaponType;
            this.BaseDamage = BaseDamage;
            this.AttackSpeed = AttackSpeed;
        }

    }
}
