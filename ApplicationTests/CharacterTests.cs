using Accelerate_assignment1.Heroes;
using Accelerate_assignment1.Heroes.Classes;
using System;
using System.Collections.Generic;
using Xunit;

namespace ApplicationTests
{
    public class CharacterTests
    {
        //Setup

        //Noobs
        Mage mage = new Mage("Nicholas");
        Ranger ranger = new Ranger("Greg");
        Rouge rouge = new Rouge("Dewald");
        Warrior warrior = new Warrior("Craig");

        [Fact]
        public void Hero_CheckIfHeroIsLevel1WhenCreated_HeroLevelShouldBeEqualTo1()
        {
            int heroLevel = mage.Level;
            int expectedLevel = 1;
            Assert.Equal(expectedLevel, heroLevel);
        }

        [Fact]
        public void LevelUp_LevelUpALevel1CharacterOnce_HeroLevelShouldBeEqualTo2()
        {
            mage.LevelUp();
            int heroLevel = mage.Level;
            int expectedLevel = 2;
            Assert.Equal(expectedLevel, heroLevel);
        }

        [Fact]
        public void LevelUp_LevelUpALevel1CharacterTenTimes_HeroLevelShouldBeEqualTo11()
        {
            mage.LevelUp(10);
            int heroLevel = mage.Level;
            int expectedLevel = 11;
            Assert.Equal(expectedLevel, heroLevel);
        }

        [Fact]
        public void LevelUp_LevelUpACharacterWithNegativeNumber_ShouldThrowArgumentException()
        {
            int levelUpNumber = -5;
            Assert.Throws<ArgumentException>(() => mage.LevelUp(levelUpNumber));
        }

        [Fact]
        public void Mage_CheckIfMageIsCreatedWithProperDefaultAttributes_ShouldBe1Str1Dex8Int5Vit()
        {
            PrimaryAttributes mageStats = mage.BasePrimaryAttributes;
            PrimaryAttributes expectedStats = new PrimaryAttributes(1, 1, 8, 5);
            Assert.Equal(expectedStats, mageStats);
        }

        [Fact]
        public void Ranger_CheckIfRangerIsCreatedWithProperDefaultAttributes_ShouldBe1Str7Dex1Int8Vit()
        {
            PrimaryAttributes rangerStats = ranger.BasePrimaryAttributes;
            PrimaryAttributes expectedStats = new PrimaryAttributes(1, 7, 1, 8);
            Assert.Equal(expectedStats, rangerStats);
        }

        [Fact]
        public void Rouge_CheckIfRougeIsCreatedWithProperDefaultAttributes_ShouldBe2Str6Dex1Int8Vit()
        {
            PrimaryAttributes rougeStats = rouge.BasePrimaryAttributes;
            PrimaryAttributes expectedStats = new PrimaryAttributes(2, 6, 1, 8);
            Assert.Equal(expectedStats, rougeStats);
        }

        [Fact]
        public void Warrior_CheckIfWarriorIsCreatedWithProperDefaultAttributes_ShouldBe5Str2Dex1Int10Vit()
        {
            PrimaryAttributes warriorStats = warrior.BasePrimaryAttributes;
            PrimaryAttributes expectedStats = new PrimaryAttributes(5, 2, 1, 10);
            Assert.Equal(expectedStats, warriorStats);
        }

        [Fact]
        public void Warrior_CheckIfWarriorIsCreatedWithProperTotalAttributes_ShouldBe5Str2Dex1Int10Vit()
        {
            PrimaryAttributes warriorStats = warrior.TotalPrimaryAttributes;
            PrimaryAttributes expectedStats = new PrimaryAttributes(5, 2, 1, 10);
            Assert.Equal(expectedStats, warriorStats);
        }

        [Fact]
        public void Mage_CheckIfLevelUpFrom1To2AddsProperBaseStats_ShouldBe2Str2Dex13Int8Vit()
        {
            mage.LevelUp();
            PrimaryAttributes mageStats = mage.BasePrimaryAttributes;
            PrimaryAttributes expectedStats = new PrimaryAttributes(2, 2, 13, 8);
            Assert.Equal(expectedStats, mageStats);
        }

        [Fact]
        public void Ranger_CheckIfLevelUpFrom1To2AddsProperBaseStats_ShouldBe2Str12Dex2Int10Vit()
        {
            ranger.LevelUp();
            PrimaryAttributes rangerStats = ranger.BasePrimaryAttributes;
            PrimaryAttributes expectedStats = new PrimaryAttributes(2, 12, 2, 10);
            Assert.Equal(expectedStats, rangerStats);
        }

        [Fact]
        public void Rouge_CheckIfLevelUpFrom1To2AddsProperBaseStats_ShouldBe3Str10Dex2Int11Vit()
        {
            rouge.LevelUp();
            PrimaryAttributes rougeStats = rouge.BasePrimaryAttributes;
            PrimaryAttributes expectedStats = new PrimaryAttributes(3, 10, 2, 11);
            Assert.Equal(expectedStats, rougeStats);
        }

        [Fact]
        public void Warrior_CheckIfLevelUpFrom1To2AddsProperBaseStats_ShouldBe8Str4Dex2Int15Vit()
        {
            warrior.LevelUp();
            PrimaryAttributes warriorStats = warrior.BasePrimaryAttributes;
            PrimaryAttributes expectedStats = new PrimaryAttributes(8, 4, 2, 15);
            Assert.Equal(expectedStats, warriorStats);
        }

        [Fact]
        public void SecondaryAttributes_CheckSecondaryAttributesOnNakedLevel2Warrior_ShouldBe150Health12ArmorRating2ElementalResistance()
        {
            warrior.LevelUp();
            SecondaryAttributes warriorStats = warrior.SecondaryAttributes;
            SecondaryAttributes expectedStats = new SecondaryAttributes(150, 12, 2);
            Assert.Equal(expectedStats, warriorStats);
        }


    }
}
