﻿using Accelerate_assignment1.Heroes.Classes;
using Accelerate_assignment1.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Accelerate_assignment1.utility;
using Accelerate_assignment1.Heroes;

namespace ApplicationTests
{
    public class ItemTests
    {
        //setup
        Warrior warrior = new Warrior("Craig");

        [Fact]
        public void EquipWeapon_AttemptToEquipLevel2AxeOnLevel1Warrior_ShouldThrowInvalidWeaponException()
        {
            Weapon axe = new Weapon("Black Cleaver", 2, WeaponTypes.Axes, 5, 1.1);
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(axe));
        }

        [Fact]
        public void EquipArmor_AttemptToEquipLevel2PlateArmorOnLevel1Warrior_ShouldThrowInvalidArmorException()
        {
            Armor plateBody = new Armor("Stoneplate", 2, ArmorTypes.Plate,
                Accelerate_assignment1.Heroes.Items.SlotTypes.Body, 5, 1, 1, 20);

            Assert.Throws<InvalidWeaponException>(() => warrior.EquipArmor(plateBody));
        }

        [Fact]
        public void EquipWeapon_AttemptToEquipBowOnWarrior_ShouldThrowInvalidWeaponException()
        {
            Weapon bow = new Weapon("Recurve bow", 1, WeaponTypes.Bows, 8, 0.8);
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(bow));
        }

        [Fact]
        public void EquipArmor_AttemptToClothArmorOnWarrior_ShouldThrowInvalidArmorException()
        {
            Armor clothBody = new Armor("Cloth armor", 1, ArmorTypes.Cloth,
                Accelerate_assignment1.Heroes.Items.SlotTypes.Body, 1, 1, 8, 6);

            Assert.Throws<InvalidWeaponException>(() => warrior.EquipArmor(clothBody));
        }

        [Fact]
        public void EquipWeapon_AttemptToEquipValidWeaponOnWarrior_ShouldGiveConfirmingString()
        {
            Weapon sword = new Weapon("Infinity edge", 1, WeaponTypes.Swords, 7, 1.5);
            string message = warrior.EquipWeapon(sword);
            string expectedMessage = "New weapon equipped!";
            Assert.Equal(message, expectedMessage);
        }

        [Fact]
        public void EquipWeapon_AttemptToEquipValidArmorOnWarrior_ShouldGiveConfirmingString()
        {
            Armor mailArmor = new Armor("Chainmail", 1, ArmorTypes.Mail,
                Accelerate_assignment1.Heroes.Items.SlotTypes.Body, 9, 1, 1, 19);
            string message = warrior.EquipArmor(mailArmor);
            string expectedMessage = "New armor equipped!";
            Assert.Equal(message, expectedMessage);
        }

        [Fact]
        public void CalculateHeroDPS_CalculateDPSWhenWeaponSlotIsEmpty_ShouldBeCloseTo1()
        {
            double heroDPS = DPSCalculator.CalculateHeroDPS(warrior);
            double expectedDPS = 1 * ((1 + warrior.TotalPrimaryAttributes.GetStat(warrior.MainStat)) / 100);
            Assert.Equal(heroDPS, expectedDPS);
        }

        [Fact]
        public void CalculateHeroDPS_CalculateDPSWithValidWeaponWith7DamageAnd1Point1AttackSpeeed_ShouldBeEqualToFormula()
        {
            Weapon axe = new Weapon("Tiamat", 1, WeaponTypes.Axes, 7, 1.1);
            warrior.EquipWeapon(axe);
            double heroDPS = DPSCalculator.CalculateHeroDPS(warrior);
            double expectedDPS = (7 * 1.1)  * ((1 + warrior.TotalPrimaryAttributes.GetStat(warrior.MainStat)) / 100);
            Assert.Equal(heroDPS, expectedDPS);
        }

        [Fact]
        public void CalculateHeroDPS_CalculateDPSWithValidWeaponWith7DamageAnd1Point1AttackSpeeedAnd1StrengthArmor_ShouldBeEqualToFormula()
        {
            Weapon axe = new Weapon("Tiamat", 1, WeaponTypes.Axes, 7, 1.1);
            warrior.EquipWeapon(axe);
            Armor plateBody = new Armor("Stoneplate", 1, ArmorTypes.Plate,
                Accelerate_assignment1.Heroes.Items.SlotTypes.Body, 1, 1, 1, 1);
            double heroDPS = DPSCalculator.CalculateHeroDPS(warrior);
            double expectedDPS = (7 * 1.1) * ((1 + warrior.TotalPrimaryAttributes.GetStat(warrior.MainStat)) / 100);
            Assert.Equal(heroDPS, expectedDPS);
        }
    }
}
